package com.se.apc.util;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

public class BrowserStack2 {

  public static final String USERNAME = "anil590";
  public static final String AUTOMATE_KEY = "yyB9TjwzyGyAcssWKSiR";
  public static final String URL = "http://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

  public static void main(String[] args) throws Exception {

    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("browserName", "iPhone");
    caps.setCapability("device", "iPhone 8 Plus");
  //  caps.setCapability("os_version", "7.0");
   // caps.setCapability("device", "Samsung Galaxy S8");
    caps.setCapability("realMobile", "true");
   // caps.setCapability("os_version", "11");
    caps.setCapability("browserstack.local", "true");
    caps.setCapability("acceptSslCerts", "true");

    System.getProperties().put("http.proxyHost", "165.225.106.32");
    System.getProperties().put("http.proxyPort", "80");


    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
    driver.get("https://intermediary.apc.com");
    Thread.sleep(10000);
    WebElement element = driver.findElement(By.xpath(".//div[@class='global-search__pseudo-submit']"));

    element.sendKeys("BK350");
    //Keys.ENTER();

    System.out.println(driver.getTitle());
    driver.quit();

  }
}