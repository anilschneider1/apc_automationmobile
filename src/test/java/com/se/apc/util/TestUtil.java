package com.se.apc.util;


import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

public class TestUtil {
	static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
	public static Object[][] getdata(String testCaseName, Xls_Reader xls){
		
		// This will read data from excel for respective test case
		// > Find row num on which test is starting
		// > Find total number of rows of data
		// > Find total number of columns of data
		// > Extract data
		
		
		// Find row num on which test is starting
		int testStartRowNum = 1;
		while(!xls.getCellData("Data", 0, testStartRowNum).equalsIgnoreCase(testCaseName)){
			testStartRowNum++;
			// This will iterate over each and every cell in column 0 till it finds the testcaseName
		}
		
		APPLICATION_LOGS.debug("Test Case "+testCaseName+ " starts from row : " + testStartRowNum);	
		
		
		// Find total number of rows of data
		int dataStartsRowNum = testStartRowNum + 2;
		int rowsOfData = 0;
		while(!xls.getCellData("Data", 0, dataStartsRowNum+rowsOfData).equals("")){
			rowsOfData++;
		}
		
		APPLICATION_LOGS.debug("Total data sets for test case "+testCaseName+ " are "+ rowsOfData);
		
		
		// Find total number of columns of data
		int ColStartRowNum = testStartRowNum + 1;
		int ColsOfData = 0;
		while(!xls.getCellData("Data", ColsOfData, ColStartRowNum).equals("")){
			ColsOfData++;
		}
		APPLICATION_LOGS.debug("Total number of Parameters for test case "+testCaseName+ " are "+ ColsOfData);
		
		
		// Extract data into HashTables and put one hash table in one single row of first column
		Object testdata[][] = new Object[rowsOfData][1];
		Hashtable<String, String> table = null; 
		int index = 0;
		
		for(int rNum = dataStartsRowNum; rNum<dataStartsRowNum+rowsOfData; rNum++){
			table = new Hashtable<String, String>();
			String forloging = "   ";
			for(int cNum = 0;cNum<ColsOfData;cNum++){
				String ParameterName = xls.getCellData("Data",cNum,ColStartRowNum);
				String ParameterValue = xls.getCellData("Data",cNum, rNum);
				forloging = forloging+" " +ParameterName+" -------- "+ParameterValue +"-------";
				//System.out.print(ParameterValue+" -------- ");
				table.put(ParameterName, ParameterValue);
			}
			//forloging = forloging + " ";
			APPLICATION_LOGS.debug(forloging);
			testdata[index][0] = table; // putting table in testdata array starting from index 0 
			index++;
		//to list 
			/*List<String> excellist = new ArrayList<String>(table.values());
			System.out.println(excellist);*/
		}
		
		return testdata;
	}
	
	 

	
	
	
	public static boolean getRunmode(String testName, Xls_Reader xls){
		
		// This will Iterate through every row of test cases sheet and check for the testName.
		// If found it will check its runmode and return boolean value or else return false by default.
		
		for(int rNum = 2; rNum<=xls.getRowCount("Test Cases");rNum++){
			String testCaseName = xls.getCellData("Test Cases", "TCID", rNum);
			if(testCaseName.equalsIgnoreCase(testName)){
				// check runmode
				if(xls.getCellData("Test Cases", "Runmode", rNum).equalsIgnoreCase("Y")){
					return true;
				}else {
					return false;
				}
					
			}
				
		}
		
		
		return false;
	}
	
	

	// update results for a particular data set	
	public static void reportDataSetResult(Xls_Reader xls, String testCaseName, int rowNum,String result){	
		xls.setCellData(testCaseName, "Results", rowNum, result);
	}

	
}
