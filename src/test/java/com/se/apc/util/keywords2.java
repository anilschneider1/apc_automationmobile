package com.se.apc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//import junit.framework.TestCase;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
//import org.apache.System.out.println4j.BasicConfigurator;
//import org.apache.System.out.println4j.System.out.printlnger;
//import org.apache.System.out.println4j.BasicConfigurator;
//import org.apache.System.out.println4j.System.out.printlnger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.se.apc.util.TestUtil;

// Selenium layer

public class keywords2 {

	public WebDriver driver = null;
	Properties OR = null; // Initialize Properties
	Properties ENV = null;
	static keywords2 keyobject;
	WebDriver backup_mozilla;
	WebDriver backup_chrome;
	WebDriver backup_ie;
	Map<String, String> comparedData = new HashMap<String, String>();

	// logger APPLICATION_LOGS =
	// System.out.printlnger.getSystem.out.printlnger("devpinoySystem.out.printlnger");
	// BasicConfigurator.configure();
	static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");

	@SuppressWarnings("unused")
	private keywords2() { // Constructor is made Private

		OR = new Properties();
		ENV = new Properties();
		driver = null;
		try {

			FileInputStream fs = new FileInputStream(System.getProperty("user.dir")
					+ "\\src\\test\\java\\com\\Schneider\\PES_V2_Automation\\config\\OR.properties");
			// FileInputStream fs = new FileInputStream("OR.properties");
			if (fs == null) {
				System.out.println("fs is null");
			} else
				OR.load(fs);
			// Init ENV Properties

			String filename = OR.getProperty("environment") + ".properties";
			System.out.println("Environment Filename :" + filename);
			APPLICATION_LOGS.debug("Environment Filename :" + filename);
			fs = new FileInputStream(System.getProperty("user.dir")
					+ "\\src\\test\\java\\com\\Schneider\\PES_V2_Automation\\config\\" + filename);
			// fs = new FileInputStream(filename);
			ENV.load(fs);
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("Unbale to load file input stream");
			APPLICATION_LOGS.debug("Unbale to load file input stream");
		}
	}

	public void logs(String mesg) {

		APPLICATION_LOGS.debug(mesg);
	}

	public static keywords2 getInstance() {
		// This will create instance of Keywords class

		if (keyobject == null)
			keyobject = new keywords2();
		return keyobject;
	}

	public String closeDriver() {
		// This will close driver instances
		try {
			// driver.close();
			driver.quit();
			return "Pass";
		} catch (Exception e) {
			return "Fail";
		}
	}

	public void openbrowser(String browser) {
		// This will open browser
		logs("Selected browser :" + browser);
		System.out.println("Selected browser :" + browser);
		if (browser.equalsIgnoreCase("Mozilla") && backup_mozilla != null) {
			driver = backup_mozilla;
			driver.manage().deleteAllCookies();
			return;
		} else if (browser.equalsIgnoreCase("Chrome") && backup_chrome != null) {
			driver = backup_chrome;
			driver.manage().deleteAllCookies();
			return;
		} else if (browser.equalsIgnoreCase("IE") && backup_ie != null) {
			driver = backup_ie;
			driver.manage().deleteAllCookies();
			return;
		}

		try {
			// driver = new FirefoxDriver();
			if (browser.equalsIgnoreCase("Mozilla")) {
				System.out.println("Running Scripts on Mozilla FireFox!");
				logs("Running Scripts on Mozilla FireFox!");
				driver = new FirefoxDriver();
				backup_mozilla = driver;

			} else if (browser.equalsIgnoreCase("IE")) {
				System.out.println("Running Scripts on Internet Explorer!");
				logs("Running Scripts on Internet Explorer!");
				System.setProperty("webdriver.ie.driver",
						System.getProperty("user.dir") + "\\drivers\\IEDriverServer.exe");
				// driver = new InternetExplorerDriver();
				backup_ie = driver;
				try {
					Runtime.getRuntime().exec("RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 255");
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else if (browser.equalsIgnoreCase("Chrome")) {
				System.out.println("Running Scripts on Chrome!");
				logs("Running Scripts on Chrome!");
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
				driver = new ChromeDriver();
				backup_chrome = driver;

			} else
				System.out.println(browser + " Browser not supported!!!");
			logs(browser + " Browser not supported!!!");
			driver.manage().deleteAllCookies();
			System.out.println("Cookies deleted...");
			logs("Cookies deleted...");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			// return "Pass";
		} catch (Throwable t) {
			System.out.println("Unable to open browser" + browser);
			// ErrorUtil.addVerificationFailure(t);
			// return "Fail";
		}
	}

	public String navigate() {
		// This will navigate to the URL
		System.out.println("Value in env variable : " + ENV.getProperty("baseURL"));
		logs("Value in env variable : " + ENV.getProperty("baseURL"));
		driver.get(ENV.getProperty("baseURL"));
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		return "Pass";
	}

	public String navigate(String url) {
		// This will navigate to the URL
		System.out.println("Value in env variable : " + ENV.getProperty(url));
		logs("Value in env variable : " + ENV.getProperty(url));
		driver.get(ENV.getProperty(url));
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		return "Pass";
	}

	public String select_dropdownapc(String country, String xpathkey) {
		// This will select value from dropdown
		logs("Country to be selected from Dropdown : " + country);
		try {
			String allcountries = driver.findElement(By.xpath(OR.getProperty(xpathkey))).getText();
			// log("All countries are : "+ allcountries);
			Select dropbox = new Select(driver.findElement(By.xpath(OR.getProperty(xpathkey))));
			dropbox.selectByVisibleText(country);
			Assert.assertTrue(true, "Country selected from dropdown");

		} catch (Exception e) {
			logs("Country " + country + " is not available in dropdown options...exiting test now!!");
			driver.close();
		}
		try {
			boolean gonogo = driver.findElement(By.xpath(OR.getProperty("GoButton"))).isDisplayed();
			if (gonogo)
				driver.findElement(By.xpath(OR.getProperty("GoButton"))).click();
		} catch (Throwable t) {
			// For I.E
			// ErrorUtil.addVerificationFailure(t);
		}

		return "Pass";
	}

	public String hovermouse(String xpathkey) {
		// This will hovermouse
		Actions builder = new Actions(driver);
		try {
			WebElement tagElement = driver.findElement(By.xpath(OR.getProperty(xpathkey)));
			builder.moveToElement(tagElement).build().perform();
			// System.out.println("Mouse Hover Done");
			Reporter.log("Mouse Hover Done", true);
			// driver.manage().wait(10);
			// log("wait over");
			return "Pass";

		} catch (Throwable t) {

			// System.out.println("Unable to mouse hover");
			Assert.fail("Unable to mouse hover");
			// ErrorUtil.addVerificationFailure(t);
			return "Fail";
		}
	}

	public String select_dropdown(String country, String xpathkey) {
		// This will select value from dropdown
		// System.out.println("Country to be selected from Dropdown : "+
		// country);
		Reporter.log("Country to be selected from Dropdown : " + country, true);
		logs("Country to be selected from Dropdown : " + country);
		String flag1;
		try {
			String allcountries = driver.findElement(By.xpath(OR.getProperty(xpathkey))).getText();
			// System.out.println("All countries are : "+ allcountries);
			Select dropbox = new Select(driver.findElement(By.xpath(OR.getProperty(xpathkey))));
			dropbox.selectByVisibleText(country);
			Assert.assertTrue(true, "Country selected from dropdown");
			driver.findElement(By.id("countryMap_dd_search_img")).click();
			flag1 = "Pass";

		} catch (Exception e) {
			// System.out.println("Country "+country+" is not available in
			// dropdown options...exiting test now!!");
			Assert.fail("Country " + country + " is not available in dropdown options...exiting test now!!");
			logs("Country " + country + " is not available in dropdown options...exiting test now!!");
			driver.close();
			flag1 = "Fail";

		}

		try {

			boolean gonogo = driver.findElement(By.xpath(OR.getProperty("GoButton"))).isDisplayed();
			if (gonogo)
				driver.findElement(By.xpath(OR.getProperty("GoButton"))).click();

		} catch (Throwable t) {
			// For I.E
			// ErrorUtil.addVerificationFailure(t);
		}
		return flag1;
	}

	public String click(String xpathkey) {
		// This will click
		try {

			driver.findElement(By.xpath(OR.getProperty(xpathkey))).click();
			// System.out.println("Click done on " + xpathkey);
			Reporter.log("Click done on " + xpathkey, true);
			logs("Click done on " + xpathkey);
			return "Pass";
		} catch (Exception e) {
			// System.out.println("Not found :" + xpathkey);
			Assert.fail("Not found :" + xpathkey);
			logs("Not found :" + xpathkey);
			return "Fail";
		}

	}

	public String validateUrl() {
		String flag;
		String actualUrl = driver.getCurrentUrl();
		String currenturl[] = actualUrl.split("/");
		String expectedUrl = ENV.getProperty("Partner_StageURL");
		int result = currenturl[2].compareToIgnoreCase(expectedUrl);
		if (result == 0) {
			// System.out.println("You are on Partners Stage site of "+
			// OR.getProperty("environment") + " environment ");
			Reporter.log("You are on Partners Stage site of " + OR.getProperty("environment") + " environment ", true);
			logs("You are on Partners Stage site of " + OR.getProperty("environment") + " environment ");
			flag = "Pass";
			return flag;
		} else {
			// System.out.println("Error! You are NOT on Partners Stage site of
			// "+ OR.getProperty("environment") + " environment ");
			Reporter.log(
					"Error! You are NOT on Partners Stage site of " + OR.getProperty("environment") + " environment ",
					true);
			logs("Error! You are NOT on Partners Stage site of " + OR.getProperty("environment") + " environment ");

			try {
				Assert.fail("Error! You are NOT on Partners Stage site of " + OR.getProperty("environment")
						+ " environment ");
			} catch (Throwable t) {
				// exception caught
			}
			System.out.println("Hence Exiting test now!!!");
			Reporter.log("Hence Exiting test now!!!", true);
			logs("Hence Exiting test now!!!");
			flag = "Fail";
			driver.close();
			return flag;
		}

	}

	public String fnSwitchWindow(String xpathkey) {
		try {
			String mainWindow = driver.getWindowHandle();
			driver.findElement(By.xpath(OR.getProperty(xpathkey))).click();
			Set<String> handles = driver.getWindowHandles();
			for (String handle : handles) {
				if (!handle.equals(mainWindow)) {
					driver.switchTo().window(handle);
					System.out.println("Tab Switched");
				}
			}
			return "Pass";
		} catch (Exception e) {
			Assert.fail("Could not switch the Window");
			return "Fail";
		}
	}

	public String fnSwitchtoMainwindow() {
		try {
			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.close();
			driver.switchTo().window(tabs2.get(0));
			return "Pass";
		} catch (Exception e) {
			Assert.fail("Could not switch the Window");
			return "Fail";
		}
	}

	public String fnSwitchIframe1() {
		try {
			driver.switchTo().frame("_oneprmprogramsresources_WAR_oneprmprogramsresourcesportlet_iframe");
			// driver.switchTo().frame(1);
			Reporter.log("IFrame Switched", true);
			return "Pass";
		} catch (Exception e) {
			Assert.fail("IFrame not switched");
			return "Fail";
		}
	}

	public String fnSwitchIframe2() {
		try {
			// driver.switchTo().frame("_oneprmprogramsresources_WAR_oneprmprogramsresourcesportlet_iframe");
			driver.switchTo().frame(1);
			Reporter.log("IFrame Switched", true);
			return "Pass";
		} catch (Exception e) {
			Assert.fail("IFrame not switched");
			return "Fail";
		}
	}

	public void fnSwitchDefaultIframe() {
		driver.switchTo().defaultContent();
	}

	public String fnGetText(String xpath) {
		try {
			String name = driver.findElement(By.xpath(OR.getProperty(xpath))).getText();
			System.out.println(name);
			Reporter.log(name, true);
			return "Pass";

		} catch (Exception e) {
			Assert.fail("Error in the verification page");
			return "Fail";
		}
	}

	public String fnGetValue(String xpath, String text) {
		try {
			if (driver.findElement(By.xpath(OR.getProperty(xpath))).isDisplayed()) {
				Reporter.log(text + " : ", true);
				String name = driver.findElement(By.xpath(OR.getProperty(xpath))).getAttribute("value");
				Reporter.log(name, true);
				return "Pass";
			} else
				return "element not found ";
		} catch (Exception e) {
			Assert.fail("Error in fetching the Value of : " + text);
			return "Fail";
		}
	}

	public String fnopennewtab() {
		try {
			driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
			return "Pass";
		} catch (Exception e) {
			Assert.fail("Error in opening a new tab");
			return "Fail";
		}
	}

	public String LiferayAdminControlPanel() {
		try {
			driver.findElement(By.xpath("//li[@id='_145_mySites']/a/span")).click();
			driver.findElement(By.xpath("//li[@id='_145_mySites']/a/span")).click();
			driver.findElement(By.className("site-name")).click();
			return "Pass";
		} catch (Exception e) {
			Assert.fail("Error in clicking on Control Panel");
			return "Fail";
		}
	}

	public String browsesite() {

		try {
			// driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS
			// );
			driver.findElement(By.xpath(OR.getProperty("NetworkTab"))).click();
			Reporter.log("Network Connectivity Sysytem Integrators tab clicked", true);
			logs("Network Connectivity Sysytem Integrators tab clicked");

			/*
			 * 
			 * Can switch between the Frames but can not click on the elements
			 * of the Iframe as the elements are hidden.
			 * 
			 * 
			 * driver.switchTo().frame(
			 * "_oneprmprogramsresources_WAR_oneprmprogramsresourcesportlet_iframe"
			 * ); System.out.println("Frame switching done");
			 * 
			 * driver.switchTo().defaultContent();
			 * driver.findElement(By.xpath(OR.getProperty("logoutButton"))).
			 * click(); System.out.println("Partner Logout done");
			 * Thread.sleep(1000L);
			 * driver.findElement(By.xpath(OR.getProperty("Products&Services")))
			 * .click(); System.Threading.Thread.Sleep(1000);
			 * 
			 * System.out.println("Product & Services tab clicked");
			 * driver.switchTo().defaultContent();
			 */
			return "Pass";

		} catch (Exception e) {
			System.out.println("Products&Services not found");
			driver.switchTo().defaultContent();
			return "Fail";
		}

	}

	public String input(String xpathkey, String inputtext) {
		// This will handle text input fields
		try {
			driver.findElement(By.xpath(OR.getProperty(xpathkey))).clear();
			driver.findElement(By.xpath(OR.getProperty(xpathkey))).sendKeys(inputtext);
			return "Pass";
		} catch (Exception e) {
			// System.out.println("Input box for entering "+ xpathkey +"not
			// found!");
			Assert.fail("Input box for entering " + xpathkey + "not found!");
			logs("Input box for entering " + xpathkey + "not found!");
			e.printStackTrace();
			return "Fail";
		}

	}

	public String fnDocumentSearch(String text) {
		try {
			String all = driver.findElement(By.name("searchAttributeFilter")).getText();
			Reporter.log("All dropdown choices are : " + all, true);
			// driver.findElement(By.xpath(OR.getProperty("Document_SearchBox"))).sendKeys(text);
			driver.findElement(By.name("keywordForm")).sendKeys(text);
			Reporter.log("You search : " + text, true);
			driver.findElement(By.xpath(OR.getProperty("DocumentSearch_Button"))).click();
			String value = driver.findElement(By.xpath(OR.getProperty("Document_Count"))).getText();
			System.out.println(value);
			return "Pass";
		} catch (Exception e) {
			Assert.fail("Error in Search on Documents");
			return "Fail";
		}
	}

	public String fnToolsSearch(String text) {
		try {
			String all = driver.findElement(By.name("searchAttributeFilter")).getText();
			Reporter.log("All dropdown choices are : " + all, true);
			driver.findElement(By.xpath(OR.getProperty("Tools_Search"))).sendKeys(text);
			driver.findElement(By.xpath(OR.getProperty("ToolsSearch_Button"))).click();
			Reporter.log("You search : " + text, true);
			String value = driver.findElement(By.xpath(OR.getProperty("Tool_Count"))).getText();
			System.out.println(value);
			return "Pass";

		} catch (Exception e) {
			Assert.fail("Error in Search on Documents");
			return "Fail";
		}
	}

	public String fnDocumentdownload() {
		try {
			String mainWindow = driver.getWindowHandle();
			driver.findElement(By.xpath("//*[@id='download-links']/a")).click();
			Thread.sleep(1000);
			Set<String> handles = driver.getWindowHandles();
			for (String handle : handles) {
				if (!handle.equals(mainWindow)) {
					driver.switchTo().window(handle);
					System.out.println("Tab Switched");
				}
			}
			Thread.sleep(2000);
			return "Pass";
		} catch (Exception e) {
			Assert.fail("Error in Document Download");
			return "Fail";
		}
	}

	public String dropdownMenu(String xpathkey, String valueToSelect) {

		// System.out.println("Vaule to be selected in dropdownMenu : "+
		// valueToSelect);
		Reporter.log("Value to be selected in dropdownMenu : " + valueToSelect, true);
		logs("Value to be selected in dropdownMenu" + valueToSelect);
		try {
			String alldropboxValues = driver.findElement(By.xpath(OR.getProperty(xpathkey))).getText();
			// System.out.println("Dropdown Values are : "+alldropboxValues);
			Reporter.log("Dropdown Values are : " + alldropboxValues, true);
			Select dropbox = new Select(driver.findElement(By.xpath(OR.getProperty(xpathkey))));
			dropbox.selectByVisibleText(valueToSelect);
			// System.out.println("Vaule selected from dropdown menu :"
			// +valueToSelect );
			Reporter.log("Value selected from dropdown menu :" + valueToSelect, true);
			logs("Value selected from dropdown menu :" + valueToSelect);
			Assert.assertTrue(true, "Value selected from dropdown");
			return "Pass";
		} catch (Throwable t) {
			// System.out.println("Error! Value "+valueToSelect+" is not
			// available in dropdown options");
			Assert.fail("Error! Value " + valueToSelect + " is not available in dropdown options");
			logs("Error! Value " + valueToSelect + " is not available in dropdown options");
			// ErrorUtil.addVerificationFailure(t);
			return "Fail";
		}
	}

	public String assertionmethod1(String object1, String expectedstr) {

		try {

			String ele = driver.findElement(By.xpath(OR.getProperty(object1))).getText();
			System.out.print("assertmethod           " + ele);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Assert.assertEquals(expectedstr, ele);
			Reporter.log("Elelments are equal ", true);
			// System.out.print("\n ");

			return "pass";

		} catch (Exception e) {
			System.out.println("obj not found");
			return "Fail";
		}
	}

	public void assertionmethod(String object1, String expectedstr) {

		String ele = driver.findElement(By.xpath(OR.getProperty(object1))).getText();
		System.out.print("assertmethod value          " + ele);
		System.out.println("expected value " + expectedstr);

		Assert.assertEquals(ele, expectedstr, "values are equal ");
		// Assert.assertEquals("Variable speed drives",
		// driver.findElement(By.id("tabLink0")).getText());
		Reporter.log("expected element is" + ele, true);
		Reporter.log("Actual element is " + expectedstr, true);
		Reporter.log("Elelments are equal ", true);
		// System.out.print("\n ");

		// return "pass";

	}

	public String fnwait() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		return "Pass";
	}

	public List<String> getListOfPageElems(String xpath) {
		List<String> listOfProductBusiness = new ArrayList<String>();
		String productBusinessName;

		System.out.println("XPath is :: " + xpath);

		try {
			// List<WebElement> list = driver.findElements(By.xpath(xpath));
			List<WebElement> list = driver.findElements(By.xpath(OR.getProperty(xpath)));// Constants.PathConstants.productBusinessName

			System.out.println(list.size());
			// List <WebElement> menu = driver.findElements(By.xpath(xpath));

			for (int j = 0; j < list.size(); j++) {
				String li = list.get(j).getText();
				System.out.println(li);
				listOfProductBusiness.add(li);

				Thread.sleep(2000);
			}
			System.out.println("out side loop " + listOfProductBusiness);
			Reporter.log("Elements are " + listOfProductBusiness, true);
			// map to list

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("List of Values captured ");
		}

		return listOfProductBusiness;
	}

	public int getListOfPageElemsCount(String xpath) {
		List<String> listOfProductBusiness = new ArrayList<String>();
		String productBusinessName;

		System.out.println("XPath is :: " + xpath);

		// try {
		// List<WebElement> list = driver.findElements(By.xpath(xpath));
		List<WebElement> list = driver.findElements(By.xpath(OR.getProperty(xpath)));// Constants.PathConstants.productBusinessName

		int elementscount = list.size();
		System.out.println(list.size());
		// List <WebElement> menu = driver.findElements(By.xpath(xpath));
		/*
		 * for (int j = 0 ; j< list.size() ;j++ ) { String li =
		 * list.get(j).getText(); System.out.println(li);
		 * listOfProductBusiness.add(li);
		 * 
		 * Thread.sleep(2000); }
		 */
		System.out.println("elements count " + elementscount);
		Reporter.log("Elements count is  " + elementscount, true);
		// map to list
		/*
		 * // return elementscount; } catch (Exception e){ e.printStackTrace();
		 * //System.out.println("multiple elements not found  " ); }
		 */

		return elementscount;
	}

	public int comparesubstr(String str) {
		int value = 0;
		String substr = new StringBuffer(str).substring(new StringBuffer(str).indexOf("(", 1) + 1,
				new StringBuffer(str).indexOf(")", 1));
		try {
			value = Integer.parseInt(substr);
		} catch (Exception e) {
			value = 0;
		}

		return value;
	}

	public boolean Verify020(String Str1, String Str2) {
		int resultvalue = comparesubstr(Str2);
		int resultvalue1 = getListOfPageElemsCount(Str1);

		if (resultvalue == resultvalue1)

		{

			Reporter.log("Downloads docs and count are equal", true);

		} else
			Reporter.log("Downloads docs and count are not equal", true);
		return false;
	}

	public List<String> getListOfPagelinks(String xpath) {
		List<String> listOfProductBusiness = new ArrayList<String>();
		String productBusinessName;

		System.out.println("XPath is :: " + xpath);

		try {
			// List<WebElement> list = driver.findElements(By.xpath(xpath));
			List<WebElement> list = driver.findElements(By.xpath(OR.getProperty(xpath)));// Constants.PathConstants.productBusinessName

			System.out.println(list.size());
			// List <WebElement> menu = driver.findElements(By.xpath(xpath));

			for (int j = 0; j < list.size(); j++) {
				String li = list.get(j).getAttribute("href");

				System.out.println(li);
				listOfProductBusiness.add(li);
				Thread.sleep(2000);
			}
			System.out.println("out side loop " + listOfProductBusiness);
			// map to list

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("List of Values captured ");
		}

		return listOfProductBusiness;
	}

	public List<String> comparingpageElems(String xpath) {
		List<String> listOfProductBusiness = new ArrayList<String>();
		String productBusinessName;

		System.out.println("XPath is :: " + xpath);

		try {
			// List<WebElement> list = driver.findElements(By.xpath(xpath));
			List<WebElement> list = driver.findElements(By.xpath(OR.getProperty(xpath)));// Constants.PathConstants.productBusinessName

			System.out.println(list.size());
			// List <WebElement> menu = driver.findElements(By.xpath(xpath));

			for (int j = 0; j < list.size(); j++) {
				String li = list.get(j).getText();
				System.out.println(li);
				listOfProductBusiness.add(li);
				Thread.sleep(2000);
			}
			System.out.println("out side loop " + listOfProductBusiness);

			// map to list

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("List of Values captured ");
		}

		return listOfProductBusiness;
	}

	public List<WebElement> verifytabs(String xpath, String text1, String textn) throws Exception {
		String[] exp = { "Presentsion", "Products", "Documents and Down loads" };
		// WebElement tabs = driver.findElement(By.id("date_mon"));
		List<WebElement> list = driver.findElements(By.xpath(OR.getProperty(xpath)));

		for (WebElement we : list) {
			for (int i = 0; i < exp.length; i++) {
				if (we.getText().equals(exp[i])) {
					System.out.println("Matched");
				}
			}
		}
		return list;
	}

	public List<String> verify002(String xpath, String testCaseName, Xls_Reader xls) {

		List<String> listOfProductBusiness = new ArrayList<String>();
		// Object[][]excelList1 = TestUtil.getdata("RangePageTabs", xls);
		// System.out.println(excelList1);
		String productBusinessName;
		try {
			// List<WebElement> list = driver.findElements(By.xpath(xpath));
			List<WebElement> list = driver.findElements(By.xpath(OR.getProperty(xpath)));// Constants.PathConstants.productBusinessName
			System.out.println(list.size());
			// List <WebElement> menu = driver.findElements(By.xpath(xpath));
			for (int j = 0; j < list.size(); j++) {
				String li = list.get(j).getText();
				// System.out.println(li);
				listOfProductBusiness.add(li);
				Thread.sleep(2000);
			}
			System.out.println("out side loop " + listOfProductBusiness);
			Object testdata = (Object) TestUtil.getdata(testCaseName, xls);

			// List<String> excellist = new
			// ArrayList<String>(TestUtil.getdata(testCaseName, xls).toSring());
			System.out.println(testdata);
			// String List<String> excellist1 = new ArrayList
			// <String>(TestUtil.getdata(testCaseName, xls))
			// List<String> excellist = new ArrayList<String>(table.values());
			// System.out.println(excellist);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("List of Values captured ");
		}

		for (String s : listOfProductBusiness) {
			System.out.println(s);
		}
		return listOfProductBusiness;
	}

	/*
	 * public List getExcelistOfElems(String testcasename,xls ) { List
	 * <String>listOfProductBusiness = new ArrayList<String>(); String
	 * productBusinessName; getdata(testcaname,xls)
	 * 
	 * System.out.println("XPath is :: "+xpath);
	 * 
	 * for (int j = 0 ; j< list.size() ;j++ ) } { String li =
	 * list.get(j).getText(); System.out.println(li);
	 * listOfProductBusiness.add(li); Thread.sleep(2000); } System.out.println(
	 * "out side loop "+listOfProductBusiness); //List<WebElement> list =
	 * driver.findElement
	 * 
	 */
	public void compareList(List<String> excelList, List<String> uiList) {
		if (excelList.containsAll(uiList) && uiList.containsAll(excelList))
			System.out.println("UI Data is same as BackEnd data from exceldata");
		else
			System.out.println("UI Data is same as BackEnd data from excel data");

		// return null;
	}

	public void compareMaps(Map webServiceMap, Map uiMap) {
		// System.out.println("List from webService is >> "+webServiceList);
		// System.out.println("List from ui >> "+uiList);

		if (webServiceMap.equals(uiMap) && uiMap.equals(webServiceMap))
			System.out.println("UI Data is same as BackEnd data from WebService");
		else
			System.out.println("UI Data is not same as BackEnd data from WebService");
		// return null;
	}

	/*
	 * public static boolean isElementAvailable(String xpath, By elementSearch,
	 * int i) { if(driver.findElement(By.className(xpath))); return
	 * isElementAvailable(object, location, 20); }
	 */

	// here is where we look for the class for the first Product Box "col-md-4"
	// private static By elementSearch = By.className("col-md-4");

	public Map<String, String> compareCollection(List<String> list, Map<String, String> map) {

		Map<String, String> returnMap = new HashMap<String, String>();

		List<String> mapValues = new ArrayList<String>(map.values());

		for (String listValue : list) {

			if (listValue != null && listValue.trim() != "") {

				if (mapValues.contains(listValue)) {
					returnMap.put(listValue, new String("UI and Xls data is matching"));
					Reporter.log("Matched Values are" + listValue, true);
				} else {
					returnMap.put(listValue, new String("UI and Xls data is not matching"));
					Reporter.log("Non Matched Values are" + listValue, true);
				}
			}
		}

		return returnMap;

	}

	public boolean searchElement(String object) {
		String xpath1 = OR.getProperty(object);
		System.out.println("Search element is  :" + xpath1);

		try {
			Thread.sleep(3000);
			if (driver.findElement(By.xpath(xpath1)).isDisplayed()) {
				// System.out.println(" The email address you requested is
				// already taken. If you own this email, please use the email
				// and password to sign in, if you forget your password, then
				// you can click Forgot Password link to get your password
				// again,else please use another email address. ");
				// Reporter.log(" The image is available ", true);
				System.out.println(" The image/element is available ");
				Assert.assertTrue(true, "The image/element is available");
				Reporter.log("The image/element is available", true);
				// flag = "Pass";
				// return true;
			}
			return true;
		} catch (Exception e) {
			System.out.println("image not found");
			Reporter.log("The image/element is not available", false);
			Assert.assertFalse(false, "The image/element not available");
			return false;
		}

	}

	public String linktest(String object) {
		// String xpath1 = OR.getProperty(object);
		String xpath1 = object;
		System.out.println("link text is  :" + xpath1);

		try {
			// Thread.sleep(3000);
			if (driver.findElement(By.linkText(xpath1)).isDisplayed()) {
				driver.findElement(By.linkText(xpath1)).click();
				// System.out.println(" The email address you requested is
				// already taken. If you own this email, please use the email
				// and password to sign in, if you forget your password, then
				// you can click Forgot Password link to get your password
				// again,else please use another email address. ");
				// Reporter.log(" The image is available ", true);
				System.out.println("  link is available ");
				Assert.assertTrue(true, "link is available");
				// flag = "Pass";

			}
			return "true";
		} catch (Exception e) {
			System.out.println("link text not found");
		}
		return "false";

	}

	public void compareMaps1(Map excelMap, Map uiMap) {
		// System.out.println("List from webService is >> "+webServiceList);
		// System.out.println("List from ui >> "+uiList);

		if (excelMap.equals(uiMap) && uiMap.equals(excelMap))
			System.out.println("UI Data is same as BackEnd data from WebService");
		else
			System.out.println("UI data is not same of Backend data from WebService");
		// return null;
	}

	private int getgroupFromDoc(WebElement driver) {
		int ret = 0;

		WebElement ele1 = driver.findElement(By.className("title"));
		List<WebElement> row1 = ele1.findElements(By.tagName("tr"));

		for (WebElement detail : row1) {
			List<WebElement> tabel = detail.findElements(By.tagName("td"));

			if (tabel.size() != 2)
				continue;

			if (tabel.get(0).getText().equalsIgnoreCase("Catalog")) {
				ret = Integer.parseInt(tabel.get(1).getText());
				break;
			}
		}

		return ret;
	}

	public void quit() {
		driver.quit();
	}

	// This will read/execute keywords from Test Steps sheet.
	public void executeKeywords(String testName, Xls_Reader xls, Hashtable<String, String> table) {

		int rows = xls.getRowCount("Test Steps");
		for (int rNum = 2; rNum <= rows; rNum++) {
			String tcid = xls.getCellData("Test Steps", "TCID", rNum);
			if (tcid.equalsIgnoreCase(testName)) {
				// This if will check if the test case name passed is available
				// in TCID column

				String keyword = xls.getCellData("Test Steps", "Keyword", rNum);
				String object = xls.getCellData("Test Steps", "Object", rNum);
				String data = xls.getCellData("Test Steps", "Data", rNum);
				String result = "";
				List listresult;
				String featureFlip_Flag = xls.getCellData("Test Steps", "Feature_Flip", rNum);
				System.out.println("value in feature flip flag : " + featureFlip_Flag);
				// execute keywords
				if (featureFlip_Flag.equalsIgnoreCase("ON")) {

					// execute keywords

					switch (keyword) {
					case "openbrowser": {
						result = "Pass";
						openbrowser(table.get(data));
						System.out.println("Opening browser : " + table.get(data));
						logs("Opening browser : " + table.get(data));
						break;
					}

					/*
					 * Original : case "navigate" :{ result = navigate(); break;
					 * }
					 */

					case "navigate": {
						result = navigate(table.get(data));
						break;
					}

					case "hovermouse": {
						System.out.println("Going to hover mouse over " + table.get(data));
						result = hovermouse(table.get(data));
						break;
					}

					case "select_dropdown": {
						System.out.println("value in object : " + object);
						logs("value in object : " + object);
						result = select_dropdown(table.get(data), object);
						break;
					}
					case "select_dropdownapc": {
						System.out.println("value in object : " + object);
						logs("value in object : " + object);
						result = select_dropdownapc(table.get(data), object);
						break;
					}

					case "click": {
						// String xPath = ENV.getProperty(object);
						System.out.println(" object is :: " + object);
						result = click(object);
						break;
					}
					case "input": {
						result = input(object, table.get(data));
						break;
					}
					case "fnSwitchIframe1": {
						result = fnSwitchIframe1();
						break;
					}
					case "linktest": {
						result = linktest(object);
						break;
					}

					case "fnSwitchWindow": {
						result = fnSwitchWindow(object);
						break;
					}
					case "fnSwitchtoMainwindow": {
						result = fnSwitchtoMainwindow();
						break;
					}
					case "fnSwitchDefaultIframe": {
						fnSwitchDefaultIframe();
						break;
					}

					case "browsesite": {
						result = browsesite();
						break;
					}
					case "fnDocumentSearch": {
						result = fnDocumentSearch(table.get(data));
						break;
					}
					case "fnToolsSearch": {
						result = fnToolsSearch(table.get(data));
						break;
					}
					case "getListOfPageElemsCount": {
						String xPath = ENV.getProperty(object);
						System.out.println(" object is :: " + object + " :: " + xPath);
						int listresultcount = getListOfPageElemsCount(object);
						// THE METHOD HAS TO BE CALLED HERE
						break;
					}
					case "Verify020": {

						Verify020(object, table.get(data));
						break;

					}

					case "dropdownMenu": {
						result = dropdownMenu(object, table.get(data));
						break;
					}
					case "fnGetValue": {
						result = fnGetValue(object, table.get(data));
						break;
					}

					case "fnDocumentdownload": {
						result = fnDocumentdownload();
						break;
					}

					case "fnGetText": {
						result = fnGetText(object);
						break;
					}
					case "LiferayAdminControlPanel": {
						result = LiferayAdminControlPanel();
						break;
					}
					case "fnopennewtab": {
						result = fnopennewtab();
						break;
					}

					case "validateUrl": {
						result = validateUrl();
						System.out.println("result value :" + result);
						logs("result value :" + result);
						break;
					}
					case "assertionmethod": {
						// System.out.println(OR.getProperty(object));

						// driver.findElement(arg0)
						assertionmethod(object, table.get(data));
						// System.out.println("result :" +
						// assertionmethod(object,table.get(data)));
						// logs("result value:"+ result1);
						break;
					}
					case "fnwait": {
						// System.out.println(OR.getProperty(object));
						try {
							fnwait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						// driver.findElement(arg0)

						// fnwait();

						// System.out.println("result :" + result1);
						logs("result value:");
						break;
					}
					case "getListOfPageElems": {
						String xPath = ENV.getProperty(object);
						System.out.println(" object is :: " + object + " :: " + xPath);
						listresult = getListOfPageElems(object);
						// THE METHOD HAS TO BE CALLED HERE
						break;
					}
					case "getListOfPagelinks": {
						String xPath = ENV.getProperty(object);
						System.out.println(" object is :: " + object + " :: " + xPath);
						listresult = getListOfPagelinks(object);
						// THE METHOD HAS TO BE CALLED HERE
						break;
					}

					case "compareCollection": {
						String xPath = ENV.getProperty(object);
						System.out.println(" object is :: " + object + " :: " + xPath);
						listresult = getListOfPageElems(object);
						comparedData = compareCollection(listresult, table);
						// THE METHOD HAS TO BE CALLED HERE
						System.out.println("Compared data is :: " + comparedData);
						break;
					}

					case "searchElement": {
						// String Uiele = driver.get(data);
						// System.out.println(" object is :: "+object+" ::
						// "+xPath);
						boolean listresult1 = searchElement(object);

						break;
					}

					default:
						break;
					}

					// This will check if continue or stop test run on test step
					// failure
					if (result.equalsIgnoreCase("Pass")) {
						if (table.get(data) != null) {
							System.out.println(tcid + "-------" + keyword + "---------" + object + "-------" + data
									+ ": " + table.get(data) + "------" + result);
							logs(tcid + "-------" + keyword + "---------" + object + "-------" + data + ": "
									+ table.get(data) + "------" + result);
						} else {
							System.out.println(tcid + "-------" + keyword + "---------" + object + "-------" + data
									+ "------" + result);
							logs(tcid + "-------" + keyword + "---------" + object + "-------" + data + "------"
									+ result);
						}

					} else {
						if (table.get(data) != null) {
							// System.out.println("Error"+
							// tcid+"-------"+keyword+"---------"+object+"-------"+data+
							// ": "+ table.get(data) +"------"+ result);
							// logs("Error"+
							// tcid+"-------"+keyword+"---------"+object+"-------"+data+
							// ": "+ table.get(data) +"------"+ result);
						} else {
							System.out.println("Error! " + tcid + "-------" + keyword + "---------" + object + "-------"
									+ data + "------" + result);
							// logs("Error!
							// "+tcid+"-------"+keyword+"---------"+object+"-------"+data
							// +"------"+ result);
						}

						// Testing for not showing error message

						/*
						 * if(!result.equalsIgnoreCase("Pass")){
						 * 
						 * // This will take screenshot of the error on page.
						 * try{ String fileName = tcid+"_"+keyword+rNum+".jpg";
						 * File srcFile =
						 * ((TakesScreenshot)driver).getScreenshotAs(OutputType.
						 * FILE); FileUtils.copyFile(srcFile, new
						 * File(System.getProperty("user.dir")+"//Screenshots//"
						 * +fileName)); System.out.println(
						 * "Screenshot taken of the failing screen"); logs(
						 * "Screenshot taken of the failing screen");
						 * }catch(Throwable t){
						 * //ErrorUtil.addVerificationFailure(t);
						 * System.out.println("**********Error********");
						 */}
					String proceed = xls.getCellData("Test Steps", "Proceed_on_Fail", rNum);
					if (proceed.equalsIgnoreCase("Y")) {
						// report fail and continue with the test
						try {
							Assert.fail(tcid + "-------" + keyword + "---------" + object + "-------" + data + "------"
									+ result);
						} catch (Throwable t) {
							// ErrorUtil.addVerificationFailure(t);
							// System.out.println("**********Error********");
						}
					} else {
						// report fail and stop test
						System.out.println("Error! Stopping test now" + tcid + "-------" + keyword + "---------"
								+ object + "-------" + data + "------" + result);
						logs("Error! Stoping test now" + tcid + "-------" + keyword + "---------" + object + "-------"
								+ data + "------" + result);
						Assert.fail(tcid + "-------" + keyword + "---------" + object + "-------" + data + "------"
								+ result);
					}
				}

				else {
					System.out.println(tcid + "-------" + keyword + "---------" + object + "-------" + data + "------"
							+ "Skipped");
					logs(tcid + "-------" + keyword + "---------" + object + "-------" + data + "------" + "Skipped");
					System.out.println("Feature flip flag is set to OFF position, hence skipped");
					logs("Feature flip flag is set to OFF position, hence skipped");

				}
			}
		}
	}

}
