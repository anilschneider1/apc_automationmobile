package com.se.apc.util;


import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BrowserStack {

  public static WebDriver driver;


  @BeforeClass
 // @org.testng.annotations.Parameters(value={"browser","version","platform"})
 // public void setUp(String browser, String version, String platform) throws Exception {
  public void setUp() throws MalformedURLException{
  DesiredCapabilities capability = new DesiredCapabilities();
    /*capability.setCapability("platform",platform);
    capability.setCapability("browserName", browser);
    capability.setCapability("browserVersion", version);*/
  capability.setCapability("browserName", "iPhone");
  capability.setCapability("device", "iPhone 8 Plus");
  capability.setCapability("realMobile", "true");
    capability.setCapability("os_version", "11");
    capability.setCapability("project", "P1");
    capability.setCapability("build", "1.0");
   // capability.setCapability("realMobile", "true");
   capability.setCapability("browserstack.local", "true");
    capability.setCapability("acceptSslCerts", "true");
    System.getProperties().put("http.proxyHost", "165.225.106.32");
    System.getProperties().put("http.proxyPort", "80");
    driver = new RemoteWebDriver(
      new URL("http://anil590:yyB9TjwzyGyAcssWKSiR@hub-cloud.browserstack.com/wd/hub"),
      capability);
  }
 
 /* @Test
  public void testSimple() throws Exception {
    driver.get("http://www.google.com");
    System.out.println("Page title is: " + driver.getTitle());
    Assert.assertEquals("Google", driver.getTitle());
    WebElement element = driver.findElement(By.name("q"));
    element.sendKeys("Browser Stack");
    element.submit();
    driver = new Augmenter().augment(driver);
    File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    try {
      FileUtils.copyFile(srcFile, new File("Screenshot.png"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }*/

 /* @AfterClass
  public void tearDown() throws Exception {
    driver.quit();
  }*/
}