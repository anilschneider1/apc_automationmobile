package com.se.apc.testcases;

import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.se.apc.util.TestUtil;
import com.se.apc.util.Xls_Reader;
import com.se.apc.util.keywords;

public class TC002_AddToCartTests {
	
	Xls_Reader xls = new Xls_Reader(System.getProperty("user.dir")+"\\src\\test\\java\\com\\se\\apc\\testdata\\apc_TestData.xlsx");
	
	keywords keyword = keywords.getInstance(); 
	
	@Test(dataProvider = "testData")
	public void addTocartTestCase(Hashtable<String,String> data){
		System.out.println("Starting Addtocart test");
		// First check runmode of test case
		if(!TestUtil.getRunmode("addTocartTestCase", xls)){
			System.out.println("Skipping Test as Runmode is N for test case");
			throw new SkipException("Skipping Test as Runmode is N for test case");
			
		}
		// Second check runmode of each data set 
		if(data.get("Runmode").equalsIgnoreCase("N")){
			System.out.println("Skipping as Flag is N for this data set");
			throw new SkipException("Skipping as Flag is N for this data set");
		}
		// Execute keywords from test steps excel sheet.
		keyword.executeKeywords("addTocartTestCase", xls, data);
	}
	
	 
	 
	 
	@DataProvider
	  public Object[][] testData(){
	  	
	  	return TestUtil.getdata("addTocartTestCase", xls);
	  
	  }
	 
	
	/*@AfterSuite
	public void exitTestSuite(){
		System.out.println("Finished running Test Suite :RangePage for Advanced");
		keyword.closeDriver();
		keyword.quit();
	}*/
	
}
